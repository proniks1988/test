﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace task1
{
    

    class Purchase : IComparable
    {
        public enum Day { NO_DAY, MONDAY, TUESDAY, WEDNESDAY, THURSDAY, FRIDAY, SATURDAY, SUNDAY };
        private string name;
        private double price;
        private uint number;
        private Day day;

        public Purchase()
        {
            name = "";
            price = 0.0;
            number = 0;
            day = Day.NO_DAY;
        }
        public Purchase(string name, double price, uint number, Day day)
        {
            this.name = name;
            this.price = price;
            this.number = number;
            this.day = day;
        }
        public string GetName() { return name; }
        public double GetPrice() { return price; }
        public uint GetNumber() { return number; }
        public Day GetDay() { return day; }
        public void SetDay(Day day) { this.day = day; }
        public void SetNumber(uint number) { this.number = number; }
        public void SetPrice(double price) { this.price = price; }
        public void SetName(string name) { this.name = name; }

        public override string ToString()
        {
            return "" + name + ";" + price + ";" + number + ";" + day;
        }

        public int CompareTo(object obj)
        {
            return (int)((((Purchase)obj).price) * (((Purchase)obj).number) - price * number);
        }
    }
}
