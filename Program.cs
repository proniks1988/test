﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;



namespace task1
{
    class Program
    {
        static void Main(string[] args)
        {
            const uint n = 5;
            Costs[] ob = new Costs[n];

            for (int i = 0; i < n; i++)
            {
                System.Console.WriteLine("\r\nУчастник под номером " + (i + 1));
                if (i == 2) continue;
                else if (i == n - 1) ob[i] = new Costs();
                else
                {
                    ob[i] = new Costs(InputM.GetStr("Введите имя "), InputM.GetDuob("Введите транспортные расходы "),
                        InputM.GetInt("Введите количество дней "));
                }

            }

            System.Console.WriteLine();
            foreach (var x in ob)
            {
                try
                {
                    x.Show();
                    System.Console.WriteLine();
                }
                catch (System.NullReferenceException exception)
                {
                    System.Console.WriteLine("Под этим номеров участник отсутствует\r\n");
                }
            }

            ob[n - 1].SetTrans(InputM.GetDuob("\r\nИзмените транспортные расходы 5-го участника "));

            uint sum = ob[0].GetDay() + ob[1].GetDay();
            System.Console.WriteLine(
                "\r\nОбщая продолжительность командировок двух первых участников равна " + sum + "\r\n");


            foreach (var x in ob)
            {
                System.Console.WriteLine(x);
            }



            System.Console.WriteLine("\r\n\r\n\r\n\r\n\r\nЗадача два\r\n");
       
            Thing wire = new Thing("Wire", new Material("steel", 7850), 0.03);
            System.Console.WriteLine(wire);
            wire.GetMaterial().SetDensyti(8500);
            System.Console.WriteLine("Новая масса равна " + wire.GetMass());





            System.Console.WriteLine("\r\n\r\n\r\n\r\n\r\nЗадача три\r\n");

            Purchase[] purchases = new Purchase[5];
            purchases[0] = new Purchase("Гречка", 2.55, 2500, Purchase.Day.SATURDAY);
            purchases[1] = new Purchase("Макароны", 1.58, 1200, Purchase.Day.MONDAY);
            purchases[2] = new Purchase("Сахар", 2.00, 500, Purchase.Day.MONDAY);
            purchases[3] = new Purchase("Тушенка", 3.5, 1500, Purchase.Day.SATURDAY);
            purchases[4] = new Purchase("Туалетная бумага", 0.88, 10000, Purchase.Day.SUNDAY);

            double dsum = 0.0, max = 0.0;
            Purchase.Day day = purchases[0].GetDay();
            foreach (var x in purchases)
            {
                dsum += x.GetPrice() * (double)x.GetNumber();
                double tmp = 0.0;
                foreach (var y in purchases)
                {
                    if (x.GetDay() == y.GetDay())
                        tmp += y.GetPrice() * (double)y.GetNumber();
                }
                if (tmp > max)
                {
                    max = tmp;
                    day = x.GetDay();
                }
                System.Console.WriteLine(x);
            }
            System.Console.WriteLine("\r\n\r\nСредняя стоимость покупок равна " + (dsum / purchases.Length) +
                "\r\nДень с максимальной покупкой это " + day + " итоговая сумма равна " + max);


            System.Console.WriteLine("\r\n\r\nОтсортированный массив по цене\r\n");
            Array.Sort(purchases);
            foreach (var x in purchases)
            {
                System.Console.WriteLine(x);
            }
        }
    }


}
