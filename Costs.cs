﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace task1
{

    class Costs
    {

        private const double money_day = 20.45;
        private string name;
        private double transport;
        private uint day;

        public Costs()
        {
            name = "";
            transport = 0.0;
            day = 0;
        }
        public Costs(string name, double transport, uint day)
        {
            this.name = name;
            this.transport = transport;
            this.day = day;
        }
        public void Show()
        {
            System.Console.Write("ФИО = " + name + "\r\n");
            System.Console.Write("Суточные = " + money_day + "\r\n");
            System.Console.Write("Транспорт = " + transport + "\r\n");
            System.Console.Write("Дней = " + day + "\r\n");
            System.Console.Write("Общие расходы = " + GetTotal() + "\r\n");
        }
        public override string ToString()
        {
            return "" + name + ";" + money_day + ";" + transport + ";" + day + ";" + GetTotal();
        }
        private double GetTotal()
        {
            return transport + money_day * day;
        }
        public string GetName() { return name; }
        public double GetTrans() { return this.transport; }
        public uint GetDay() { return day; }
        public void SetName(string name) { this.name = name; }
        public void SetTrans(double trans) { transport = trans; }
        public void SetDay(uint day) { this.day = day; }
    }
    class InputM
    {

        public static string GetStr(string str)
        {
            System.Console.Write(str);
            return System.Console.ReadLine();
        }
        public static double GetDuob(string str)
        {
            double d = 0.0;
            do
            {
                try
                {
                    System.Console.Write(str);
                    d = System.Convert.ToDouble(System.Console.ReadLine());
                    if (d < 0)
                    {
                        System.Console.Write("Введите положительное число\r\n");
                        continue;
                    }
                    break;
                }
                catch (System.FormatException exception)
                {
                    System.Console.Write("Введите действительное число, разделитель \",\"\r\n");
                }
            } while (true);
            return d;
        }
        public static uint GetInt(string str)
        {
            uint d = 0;
            do
            {
                try
                {
                    System.Console.Write(str);
                    d = System.Convert.ToUInt32(System.Console.ReadLine());
                    break;
                }
                catch (System.FormatException exception)
                {
                    System.Console.Write("Введите положительное натуральное число\r\n");
                }
            } while (true);
            return d;
        }


    }


}
