﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace task1
{
    class Material
    {
        private string name;
        private uint densyti;

        public Material()
        {
            name = "";
            densyti = 0;
        }
        public Material(string name, uint densyti)
        {
            this.name = name;
            this.densyti = densyti;
        }

        public string GetName() { return name; }
        public uint GetDensyti() { return densyti; }
        public void SetDensyti(uint densyti) { this.densyti = densyti; }
        public void SetName(string name) { this.name = name; }
        public override string ToString()
        {
            return "" + name + ";" + densyti;
        }
    }

    class Thing
    {
        Material material;
        string name;
        double volume;

        public Thing()
        {
            name = "";
            material = new Material();
            volume = 0.0;
        }
        public Thing(string name, Material material, double volume)
        {
            this.name = name;
            this.material = material;
            this.volume = volume;
        }
        public string GetName() { return name; }
        public double GetVolume() { return volume; }
        public Material GetMaterial() { return material; }
        public void SetMaterial(Material material) { this.material = material; }
        public void SetVolume(uint volume) { this.volume = volume; }
        public void SetName(string name) { this.name = name; }

        public double GetMass() { return volume* (double)material.GetDensyti(); }
        public override string ToString()
        {
            return "" + name + ";" + material + ";" + volume + ";" + GetMass();
        }

    }


}
